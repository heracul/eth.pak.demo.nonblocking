package eth.pak.demo.nonblocking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class NonBlockingApplication {

	public static void main(String[] args) {
		SpringApplication.run(NonBlockingApplication.class, args);
	}
}
