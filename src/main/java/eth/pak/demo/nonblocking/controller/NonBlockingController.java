package eth.pak.demo.nonblocking.controller;

import eth.pak.demo.nonblocking.service.NonBlockingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
public class NonBlockingController {

    private Logger logger = LoggerFactory.getLogger(NonBlockingController.class);
    @Autowired
    private NonBlockingService nonBlockingService;
    @RequestMapping("/")
    public ResponseEntity<String> __healthChk() {
        return ResponseEntity.ok("OK");
    }

    @RequestMapping("/sync")
    public ResponseEntity<String> block() {
        logger.debug(">>>>> Start normal");
        try {
            nonBlockingService.doSleep();
        } catch (Exception e) {
//            e.printStackTrace();
            throw e;
        }
        logger.debug(">>>>> End normal");
        return ResponseEntity.ok("OK");
    }
    @RequestMapping("/callDeferredResult")
    public DeferredResult<String> nonBlock() {
        final DeferredResult<String> deferredResult = new DeferredResult();
        logger.debug(">>>>> Start DeferredResult");
        new Thread(()->{
            try {
                nonBlockingService.doSleep();
                deferredResult.setResult("OK");
            } catch (Exception e) {
//                e.printStackTrace();
                throw e;
            }
        }).start();
        logger.debug(">>>>> End DeferredResult");
        return deferredResult;
    }
}
