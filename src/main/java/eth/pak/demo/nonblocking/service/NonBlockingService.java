package eth.pak.demo.nonblocking.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class NonBlockingService {
    private Logger logger = LoggerFactory.getLogger(NonBlockingService.class);
    public void doSleep() {
        try {
            logger.debug(">>>>> Start service");
            int random = ThreadLocalRandom.current().nextInt(1, 11);
            if (random % 5 == 0) {
                Thread.sleep(4000);
            } else {
                Thread.sleep(1000);
            }
            logger.debug(">>>>>End Service");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
